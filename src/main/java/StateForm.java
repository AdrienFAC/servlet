/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import simplejdbc.CustomerEntity;
import simplejdbc.DAO;
import simplejdbc.DAOException;
import simplejdbc.DataSourceFactory;

/**
 *
 * @author pedago
 */
@WebServlet(urlPatterns = {"/StateForm"})
public class StateForm extends HttpServlet {

    private DAO myDAO; // L'objet à tester
    private DataSource myDataSource;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DAOException {
        response.setContentType("text/html;charset=UTF-8");
        myDataSource = DataSourceFactory.getDataSource();
        myDAO = new DAO(myDataSource);
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet StateForm</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet StateForm at " + request.getContextPath() + "</h1>");
            
            out.println("<form>");
            out.println("<select name=\"lst\">"
                            + "<option value=\"CA\">CA</option>"
                            + "<option value=\"TX\">TX</option>"
                            + "<option value=\"MI\">MI</option>"
                            + "<option value=\"NY\">NY</option>"
                            + "<option value=\"FL\">FL</option>"
                        + "</select>");
            out.println("<input type=\"submit\" name=\"valid\" value=\"Valider\">");
            out.println("</form>");

            if (request.getParameter("valid") != null)
            {
                String state = request.getParameter("lst");
                
                out.println("<table style=\"width:100%\">");
                out.println("<tr>" + "<th>ID</th>" + "<th>Name</th>" + "<th>Adress</th>" + "<\tr>");
                List<CustomerEntity> customersList = myDAO.customersInState(state);
                for (CustomerEntity customer : customersList)
                    out.println("<tr>" + "<th>" + customer.getCustomerId() + "</th>" + "<th>" + customer.getName() + "</th>" + "<th>" + customer.getAddressLine1() + "</th>" + "<\tr>");
                out.println("</table>");

            }
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DAOException ex) {
            Logger.getLogger(StateForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DAOException ex) {
            Logger.getLogger(StateForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
